# Introduction To Linux

## By Tanveer Salim

<p>Did you know that Linux used to be called [Freakx](https://en.wikipedia.org/wiki/History_of_Linux)?

<br>

The person who made Linux was just your average university student who--like many college students

<br>

--wanted access to free software (Guiltily looks at his PirateBay account).

<br>

Linus Torvalds was getting tired of how expensive UNIX was getting. Back then, most UNIX

<br>

would cost **a lot** of money.

<br>

And being as broke as he was, he decided it would be easier to make his own implementation.

<br>

Its honestly a very funny tradition how coders think that its easier to actually write

<br>

their own software than try to mooch off of someone else's. Because guess what?

<br>

It actually ***feels*** [easier](https://www.joelonsoftware.com/2000/04/06/things-you-should-never-do-part-i/).

<br>

If you have ever tried to read someone else's code, you were probably nearly

<br>

compelled to start from scratch. But of course, as Joel's blog points out--**DO NOT**

<br>

do that!

<br>

Not only are you doomed to make the same mistakes like Netscape did, but you

<br>

suffer the burdens the original coders faced when they had to deal with people.

<br>

And many people have a hard time managing...people.

<br>

</p>

<p>

So when Linus Torvalds set out to repeat this mistake, he did not get that far

<br>

from a technological standpoint for that reason. What made Linux take off

<br>

is Linus Torvalds ability to get people from around the world to work on the

<br>

project enthuisiasitcally--and for a long period of time free of charge.

<br>

It was out of a business need. At the time Linux was released, businesses

<br>

were struggling to cover the maintenance costs of servers.

<br>

Companies like Microsoft were charging the **big** bucks to get people

<br>

to use their state of the art server operating systems.

<br>

Not only were those more expensive, they were also a [**lot** slower](https://linuxgazette.net/issue59/correa.html).

<br>

But most of all--more expensive.

<br>

So when Linux started out--and Linus made it free of charge to distribute,

<br>

modify, and even commercialize as anyone pleased--even startups like

<br>

Google began using the operating system to run their websites.

<br>

And this is what led to the Internet boom in the 1990s.

<br>

But the Internet became a mixed blessing--when people

<br>

noticed it was easier to mess with people's private business

<br>

with it.

<br>

And that is exactly how the Cybersecurity Industry was born :)

<br>

Today, security developers are expected to know how to use Linux

<br>

--especially since >95% of the Internet runs on it.

<br>

And the best way to get started is by downloading a good Linux 

<br>

operating system.

<br>

We at RaiderHacks recommend [Manjaro](https://manjaro.org/downloads/official/kde/) for beginners.

<br>

Its a fast, slick, OS that is going to help you get used to the world of Linux.

<br>

And with it, you will master the Linux command line.

<br>

The command line is arguably the most powerful application any Linux computer.

<br>

This is what the experts use to do all of their hacking.

<br>

It will make you faster, smarter, and more flexible with how you develop your code.

<br>

And there is perhaps no better introduction to it than the one already available

<br>

by [Joe Collins](https://www.youtube.com/watch?v=oxuRxtrO2Ag)
</p>
